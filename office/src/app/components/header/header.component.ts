import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public dialog: MatDialog) {
    // openDialog() {
    //   this.MatDialog.open(DialogElementsExampleDialog);
    // }
  }

  openModal() {
    const dialogRef = this.dialog.open(ModalComponent /* , {data: {name: 'Jonathan'}} */);
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);

    });

  }

  ngOnInit(): void {
  }

}
